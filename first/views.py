from datetime import datetime
from random import randint

from django.shortcuts import render

# Create your views here.
from first.models import CalcHistory


def get_start_context(request):
    menu = [
        {'link': '/', 'text': 'Главная'},
        {'link': '/calculator', 'text': 'Калькулятор'},
        {'link': '/calculator?a=36&b=42', 'text': 'Калькулятор 36+42'},
        {'link': '/expression', 'text': 'Выражение'},
        {'link': '/history', 'text': 'История'},
        {'link': '/question', 'text': 'Тестовая страница опроса'},
    ]
    login = []
    if not request.user.is_anonymous:
        login.append({'link': '/logout', 'text': 'Log out'})
    else:
        login.append({'link': '/login', 'text': 'Log in'})
    return {
        'menu': menu,
        'login': login,
    }


def index_page(request):
    context = get_start_context(request)
    context['date'] = datetime.now()
    return render(request, 'index.html', context)


def calculator(request):
    a = request.GET.get('a', randint(1, 99))
    b = request.GET.get('b', randint(1, 99))
    result = int(a) + int(b)
    context = get_start_context(request)
    context['first_value'] = a
    context['second_value'] = b
    context['result'] = result
    return render(request, 'calculator.html', context)


def riddle(request):
    context = get_start_context(request)
    return render(request, 'riddle.html', context)

def mobile(request):
    context = get_start_context(request)
    return render(request, 'mobile/mobile.html', context)


def auth(request):
    context = get_start_context(request)
    return render(request, 'form_auth.html', context)


def register(request):
    context = get_start_context(request)
    return render(request, 'form_register.html', context)


def answer(request):
    context = get_start_context(request)
    return render(request, 'answer.html', context)


def expression(request):
    context = get_start_context(request)
    a = randint(10, 99)
    context['expression'] = str(a)
    context['result'] = 0
    for i in range(randint(2, 4)):
        a = randint(10, 99)
        sign = '-' if randint(1, 2) == 1 else '+'
        context['expression'] += ' {} {}'.format(sign, a)
        if sign == '-':
            context['result'] -= a
        else:
            context['result'] += a
    item = CalcHistory(date=datetime.now(), expression=context['expression'], result=context['result'])
    item.save()
    return render(request, 'expression.html', context)


def question(request):
    context = get_start_context(request)
    return render(request, 'question.html', context)

def history(request):
    items = CalcHistory.objects.all()
    context = get_start_context(request)
    context['history'] = items

    return render(request, 'history.html', context)


def form(request):
    context = get_start_context(request)
    return render(request, 'form.html', context)
