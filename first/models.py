from django.db import models

# Create your models here.


class CalcHistory(models.Model):
    date = models.DateTimeField()
    expression = models.CharField(max_length=18)
    result = models.IntegerField()
